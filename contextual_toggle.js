(function($) {

Drupal.admin = Drupal.admin || {};
Drupal.admin.behaviors = Drupal.admin.behaviors || {};

/**
 * @ingroup admin_behaviors
 * @{
 */

/**
 * Apply contextual links highlighting on click.
 */
Drupal.admin.behaviors.contextuallinksToggle = function(context, settings, $adminMenu) {
  // Bind our admin button for toggling the links.
  $('div#admin-menu ul li.contextual-toggle a', context).bind('click', function() {
    $(this).toggleClass('active');
    $('div.contextual-links-region', context).toggleClass('contextual-toggle-area');
    return false;
  });

  // Turn off our highlighting when hovering the contextual regions
  
  $('div.contextual-links-region', context).hover(
    function() {
      if($('div#admin-menu ul li.contextual-toggle a', context).hasClass('active')) {
        $('div.contextual-links-region', context).removeClass('contextual-toggle-area');
      }
    },
    function() {
      if($('div#admin-menu ul li.contextual-toggle a', context).hasClass('active')) {
        $('div.contextual-links-region', context).addClass('contextual-toggle-area');
      }
    }
  );  
};

  /**
   * @} End of "defgroup admin_behaviors".
   */
})(jQuery);
